<?php
	$a00=array();
	$a01=array();
	$a02=array();
	$a03=array();
	$a04=array();
	$a05=array();
	$a06=array();
	$a07=array();
	$a08=array();
	$a09=array();
	$a10=array();
	$a11=array();
	$a12=array();
	$a13=array();
	$csv_read_trigger=false;
	$row_csv=0;
	$link=$path[$link_num].".csv";
	if(($handle_in=fopen("$link","r"))!==FALSE)
	{
		$csv_read_trigger=true;
		while(($data=fgetcsv($handle_in,1000,","))!==FALSE)
		{
			if($row_csv==0)
			{
				$price_position=$data[0];
				$last_sub_rally_price=$data[1];
				$last_prime_rally_price=$data[2];
				$last_main_rally_price=$data[3];
				$last_main_reaction_price=$data[4];
				$last_prime_reaction_price=$data[5];
				$last_sub_reaction_price=$data[6];
				$main_rally_pivot_point=$data[7];
				$prime_rally_pivot_point=$data[8];
				$main_reaction_pivot_point=$data[9];
				$prime_reaction_pivot_point=$data[10];
			};
			if($row_csv>1)
			{
				$a00[$row_csv-2]=$data[0];
				$a01[$row_csv-2]=$data[1];
				$a02[$row_csv-2]=$data[2];
				$a03[$row_csv-2]=$data[3];
				$a04[$row_csv-2]=$data[4];
				$a05[$row_csv-2]=$data[5];
				$a06[$row_csv-2]=$data[6];
				$a07[$row_csv-2]=$data[7];
				$a08[$row_csv-2]=$data[8];
				$a09[$row_csv-2]=$data[9];
				$a10[$row_csv-2]=$data[10];
				$a11[$row_csv-2]=$data[11];
				$a12[$row_csv-2]=$data[12];
				$a13[$row_csv-2]=$data[13];
			};
			$row_csv++;
		};
		fclose($handle_in);
		$last_date=$a01[0];
		$row_csv=$row_csv-2;
		$d_date=strtotime($last_date);
		if(date('D',$d_date)=="Fri")
		{
			$new_date=$d_date+86400*3;
		}
		else
		{
			$new_date=$d_date+86400;
		};
		$y_d=date("d",$new_date);
		$y_m=date("m",$new_date)-1;
		$y_y=date("Y",$new_date);
		$link="http://real-chart.finance.yahoo.com/table.csv?s=".$ticker[$link_num]."&g=d&a=".$y_m."&b=".$y_d."&c=".$y_y."&ignore=.csv";
		include "read_after_csv.php";
	};
?>