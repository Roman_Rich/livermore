<?php
	for($counter=$readed_from_yahoo_row;$counter>0;$counter--)
	{
		for($j=$counter;$j<=$counter+offset-1;$j++)
			$volume_sum=$volume_sum+$volume[$j];
		if($volume_sum!==0.0)
			$strengt_of_volume[$counter]=$volume[$counter]*offset/$volume_sum;
		else
			$strengt_of_volume[$counter]=0.0;
		$volume_sum=0.0;
		switch($price_position)
		{
			case "a":
			{
				if($high[$counter]>$last_main_rally_price)
				{
					$trigger[$counter]=1.0;
					//$sub_rally[$counter]=0.0;
					$sub_rally[$counter]="";
					//$prime_rally[$counter]=0.0;
					$prime_rally[$counter]="";
					$main_rally[$counter]=$high[$counter];
					$main_reaction[$counter]="";
					$prime_reaction[$counter]="";
					$sub_reaction[$counter]="";
					$price_position="c";
					$last_main_rally_price=$main_rally[$counter];
				}
				else
				{
					if($high[$counter]>$last_prime_rally_price)
					{
						$trigger[$counter]=1.0;
						//$sub_rally[$counter]=0.0;
						$sub_rally[$counter]="";
						$prime_rally[$counter]=$high[$counter];
						//$main_rally[$counter]=0.0;
						$main_rally[$counter]="";
						$main_reaction[$counter]="";
						$prime_reaction[$counter]="";
						$sub_reaction[$counter]="";
						$price_position="b";
						$last_prime_rally_price=$prime_rally[$counter];
					}
					else
					{
						if($high[$counter]>$last_sub_rally_price)
						{
							$trigger[$counter]=1.0;
							$sub_rally[$counter]=$high[$counter];
							//$prime_rally[$counter]=0.0;
							$prime_rally[$counter]="";
							//$main_rally[$counter]=0.0;
							$main_rally[$counter]="";
							$main_reaction[$counter]="";
							$prime_reaction[$counter]="";
							$sub_reaction[$counter]="";
							$price_position="a";
							$last_sub_rally_price=$sub_rally[$counter];
						}
						else
						{
							//$sub_rally[$counter]=0.0;
							$sub_rally[$counter]="";
							//$prime_rally[$counter]=0.0;
							$prime_rally[$counter]="";
							//$main_rally[$counter]=0.0;
							$main_rally[$counter]="";
							if(($last_sub_rally_price-$low[$counter])>($last_sub_rally_price*reverse))
							{
								if(($low[$counter]<$last_main_reaction_price)||(($prime_reaction_pivot_point-$low[$counter])>($prime_reaction_pivot_point*reverse*0.5)))
								{
									$trigger[$counter]=1.0;
									$main_reaction[$counter]=$low[$counter];
									//$prime_reaction[$counter]=0.0;
									$prime_reaction[$counter]="";
									//$sub_reaction[$counter]=0.0;
									$sub_reaction[$counter]="";
									$price_position="d";
									$last_main_reaction_price=$main_reaction[$counter];
								}
								else
								{
									if($low[$counter]<$last_prime_reaction_price)
									{
										$trigger[$counter]=1.0;
										//$main_reaction[$counter]=0.0;
										$main_reaction[$counter]="";
										$prime_reaction[$counter]=$low[$counter];
										//$sub_reaction[$counter]=0.0;
										$sub_reaction[$counter]="";
										$price_position="e";
										$last_prime_reaction_price=$prime_reaction[$counter];
									}
									else
									{
										$trigger[$counter]=1.0;
										//$main_reaction[$counter]=0.0;
										$main_reaction[$counter]="";
										//$prime_reaction[$counter]=0.0;
										$prime_reaction[$counter]="";
										$sub_reaction[$counter]=$low[$counter];
										$price_position="f";
										$last_sub_reaction_price=$sub_reaction[$counter];
									};
								};
							}
							else
							{
								$trigger[$counter]=0.0;
								//$main_reaction[$counter]=0.0;
								$main_reaction[$counter]="";
								//$prime_reaction[$counter]=0.0;
								$prime_reaction[$counter]="";
								//$sub_reaction[$counter]=0.0;
								$sub_reaction[$counter]="";
								$price_position="a";
							};
						};
					};
				};
				break;
			};
			case "b":
			{
				//$sub_rally[$counter]=0.0;
				$sub_rally[$counter]="";
				if(($high[$counter]>$last_main_rally_price)||((($high[$counter]-$prime_rally_pivot_point)>($prime_rally_pivot_point*reverse*0.5))&&($high[$counter]>$last_prime_rally_price)))
				{
					$trigger[$counter]=1.0;
					//$prime_rally[$counter]=0.0;
					$prime_rally[$counter]="";
					$main_rally[$counter]=$high[$counter];
					$main_reaction[$counter]="";
					$prime_reaction[$counter]="";
					$sub_reaction[$counter]="";
					$price_position="c";
					$last_main_rally_price=$main_rally[$counter];
				}
				else
				{
					if($high[$counter]>$last_prime_rally_price)
					{
						$trigger[$counter]=1.0;
						$prime_rally[$counter]=$high[$counter];
						//$main_rally[$counter]=0.0;
						$main_rally[$counter]="";
						$main_reaction[$counter]="";
						$prime_reaction[$counter]="";
						$sub_reaction[$counter]="";
						$price_position="b";
						$last_prime_rally_price=$prime_rally[$counter];
					}
					else
					{
						//$prime_rally[$counter]=0.0;
						$prime_rally[$counter]="";
						//$main_rally[$counter]=0.0;
						$main_rally[$counter]="";
						if(($last_prime_rally_price-$low[$counter])>($last_prime_rally_price*reverse))
						{
							$prime_rally_pivot_point=$last_prime_rally_price;
							if(($low[$counter]<$last_main_reaction_price)||(($prime_reaction_pivot_point-$low[$counter])>($prime_reaction_pivot_point*reverse*0.5)))
							{
								$trigger[$counter]=1.0;
								$main_reaction[$counter]=$low[$counter];
								//$prime_reaction[$counter]=0.0;
								$prime_reaction[$counter]="";
								//$sub_reaction[$counter]=0.0;
								$sub_reaction[$counter]="";
								$price_position="d";
								$last_main_reaction_price=$main_reaction[$counter];
							}
							else
							{
								if($low[$counter]<$last_prime_reaction_price)
								{
									$trigger[$counter]=1.0;
									//$main_reaction[$counter]=0.0;
									$main_reaction[$counter]="";
									$prime_reaction[$counter]=$low[$counter];
									//$sub_reaction[$counter]=0.0;
									$sub_reaction[$counter]="";
									$price_position="e";
									$last_prime_reaction_price=$prime_reaction[$counter];
								}
								else
								{
									$trigger[$counter]=1.0;
									//$main_reaction[$counter]=0.0;
									$main_reaction[$counter]="";
									//$prime_reaction[$counter]=0.0;
									$prime_reaction[$counter]="";
									$sub_reaction[$counter]=$low[$counter];
									$price_position="f";
									$last_sub_reaction_price=$sub_reaction[$counter];
								};
							};
						}
						else
						{
							$trigger[$counter]=0.0;
							//$main_reaction[$counter]=0.0;
							$main_reaction[$counter]="";
							//$prime_reaction[$counter]=0.0;
							$prime_reaction[$counter]="";
							//$sub_reaction[$counter]=0.0;
							$sub_reaction[$counter]="";
							$price_position="b";
						};
					};
				};
				break;
			};
			case "c":
			{
				//$sub_rally[$counter]=0.0;
				$sub_rally[$counter]="";
				//$prime_rally[$counter]=0.0;
				$prime_rally[$counter]="";
				if($high[$counter]>$last_main_rally_price)
				{
					$trigger[$counter]=1.0;
					$main_rally[$counter]=$high[$counter];
					//$main_reaction[$counter]=0.0;
					$main_reaction[$counter]="";
					//$prime_reaction[$counter]=0.0;
					$prime_reaction[$counter]="";
					//$sub_reaction[$counter]=0.0;
					$sub_reaction[$counter]="";
					$price_position="c";
					$last_main_rally_price=$main_rally[$counter];
				}
				else
				{
					//$main_rally[$counter]=0.0;
					$main_rally[$counter]="";
					if(($last_main_rally_price-$low[$counter])>($last_main_rally_price*reverse))
					{
						$main_rally_pivot_point=$last_main_rally_price;
						if(($low[$counter]<$last_main_reaction_price)||(($prime_reaction_pivot_point-$low[$counter])>($prime_reaction_pivot_point*reverse*0.5)))
						{
							$trigger[$counter]=1.0;
							$main_reaction[$counter]=$low[$counter];
							//$prime_reaction[$counter]=0.0;
							$prime_reaction[$counter]="";
							//$sub_reaction[$counter]=0.0;
							$sub_reaction[$counter]="";
							$price_position="d";
							$last_main_reaction_price=$main_reaction[$counter];
						}
						else
						{
							$trigger[$counter]=1.0;
							//$main_reaction[$counter]=0.0;
							$main_reaction[$counter]="";
							$prime_reaction[$counter]=$low[$counter];
							//$sub_reaction[$counter]=0.0;
							$sub_reaction[$counter]="";
							$price_position="e";
							$last_prime_reaction_price=$prime_reaction[$counter];
						};
					}
					else
					{
						$trigger[$counter]=0.0;
						//$main_reaction[$counter]=0.0;
						$main_reaction[$counter]="";
						//$prime_reaction[$counter]=0.0;
						$prime_reaction[$counter]="";
						//$sub_reaction[$counter]=0.0;
						$sub_reaction[$counter]="";
						$price_position="c";
					};
				};
				break;
			};
			case "d":
			{
				//$prime_reaction[$counter]=0.0;
				$prime_reaction[$counter]="";
				//$sub_reaction[$counter]=0.0;
				$sub_reaction[$counter]="";
				if($low[$counter]<$last_main_reaction_price)
				{
					$trigger[$counter]=1.0;
					//$sub_rally[$counter]=0.0;
					$sub_rally[$counter]="";
					//$prime_rally[$counter]=0.0;
					$prime_rally[$counter]="";
					//$main_rally[$counter]=0.0;
					$main_rally[$counter]="";
					$main_reaction[$counter]=$low[$counter];
					$price_position="d";
					$last_main_reaction_price=$main_reaction[$counter];
				}
				else
				{
					//$main_reaction[$counter]=0.0;
					$main_reaction[$counter]="";
					if(($high[$counter]-$last_main_reaction_price)>($last_main_reaction_price*reverse))
					{
						$main_reaction_pivot_point=$last_main_reaction_price;
						if(($high[$counter]>$last_main_rally_price)||(($high[$counter]-$prime_rally_pivot_point)>($prime_rally_pivot_point*reverse*0.5)))
						{
							$trigger[$counter]=1.0;
							//$sub_rally[$counter]=0.0;
							$sub_rally[$counter]="";
							//$prime_rally[$counter]=0.0;
							$prime_rally[$counter]="";
							$main_rally[$counter]=$high[$counter];
							$price_position="c";
							$last_main_rally_price=$main_rally[$counter];
						}
						else
						{
							$trigger[$counter]=1.0;
							//$sub_rally[$counter]=0.0;
							$sub_rally[$counter]="";
							$prime_rally[$counter]=$high[$counter];
							//$main_rally[$counter]=0.0;
							$main_rally[$counter]="";
							$price_position="b";
							$last_prime_rally_price=$prime_rally[$counter];
						};
					}
					else
					{
						$trigger[$counter]=0.0;
						//$sub_rally[$counter]=0.0;
						$sub_rally[$counter]="";
						//$prime_rally[$counter]=0.0;
						$prime_rally[$counter]="";
						//$main_rally[$counter]=0.0;
						$main_rally[$counter]="";
						$price_position="d";
					};
				};
				break;
			};
			case "e":
			{
				//$sub_reaction[$counter]=0.0;
				$sub_reaction[$counter]="";
				if(($low[$counter]<$last_main_reaction_price)||((($prime_reaction_pivot_point-$low[$counter])>($prime_reaction_pivot_point*reverse*0.5))&&($low[$counter]<$last_prime_reaction_price)))
				{
					$trigger[$counter]=1.0;
					$sub_rally[$counter]="";
					$prime_rally[$counter]="";
					$main_rally[$counter]="";
					$main_reaction[$counter]=$low[$counter];
					//$prime_reaction[$counter]=0.0;
					$prime_reaction[$counter]="";
					$price_position="d";
					$last_main_reaction_price=$main_reaction[$counter];
				}
				else
				{
					if($low[$counter]<$last_prime_reaction_price)
					{
						$trigger[$counter]=1.0;
						$sub_rally[$counter]="";
						$prime_rally[$counter]="";
						$main_rally[$counter]="";
						//$main_reaction[$counter]=0.0;
						$main_reaction[$counter]="";
						$prime_reaction[$counter]=$low[$counter];
						$price_position="e";
						$last_prime_reaction_price=$prime_reaction[$counter];
					}
					else
					{
						//$main_reaction[$counter]=0.0;
						$main_reaction[$counter]="";
						//$prime_reaction[$counter]=0.0;
						$prime_reaction[$counter]="";
						if(($high[$counter]-$last_prime_reaction_price)>($last_prime_reaction_price*reverse))
						{
							$prime_reaction_pivot_point=$last_prime_reaction_price;
							if(($high[$counter]>$last_main_rally_price)||(($high[$counter]-$prime_rally_pivot_point)>($prime_rally_pivot_point*reverse*0.5)))
							{
								$trigger[$counter]=1.0;
								//$sub_rally[$counter]=0.0;
								$sub_rally[$counter]="";
								//$prime_rally[$counter]=0.0;
								$prime_rally[$counter]="";
								$main_rally[$counter]=$high[$counter];
								$price_position="c";
								$last_main_rally_price=$main_rally[$counter];
							}
							else
							{
								if($high[$counter]>$last_prime_rally_price)
								{
									$trigger[$counter]=1.0;
									//$sub_rally[$counter]=0.0;
									$sub_rally[$counter]="";
									$prime_rally[$counter]=$high[$counter];
									//$main_rally[$counter]=0.0;
									$main_rally[$counter]="";
									$price_position="b";
									$last_prime_rally_price=$prime_rally[$counter];
								}
								else
								{
									$trigger[$counter]=1.0;
									$sub_rally[$counter]=$high[$counter];
									//$prime_rally[$counter]=0.0;
									$prime_rally[$counter]="";
									//$main_rally[$counter]=0.0;
									$main_rally[$counter]="";
									$price_position="a";
									$last_sub_rally_price=$sub_rally[$counter];
								};
							};
						}
						else
						{
							$trigger[$counter]=0.0;
							//$sub_rally[$counter]=0.0;
							$sub_rally[$counter]="";
							//$prime_rally[$counter]=0.0;
							$prime_rally[$counter]="";
							//$main_rally[$counter]=0.0;
							$main_rally[$counter]="";
							$price_position="e";
						};
					};
				};
				break;
			};
			case "f":
			{
				if($low[$counter]<$last_main_reaction_price)
				{
					$trigger[$counter]=1.0;
					$sub_rally[$counter]="";
					$prime_rally[$counter]="";
					$main_rally[$counter]="";
					$main_reaction[$counter]=$low[$counter];
					//$prime_reaction[$counter]=0.0;
					$prime_reaction[$counter]="";
					//$sub_reaction[$counter]=0.0;
					$sub_reaction[$counter]="";
					$price_position="d";
					$last_main_reaction_price=$main_reaction[$counter];
				}
				else
				{
					if($low[$counter]<$last_prime_reaction_price)
					{
						$trigger[$counter]=1.0;
						$sub_rally[$counter]="";
						$prime_rally[$counter]="";
						$main_rally[$counter]="";
						//$main_reaction[$counter]=0.0;
						$main_reaction[$counter]="";
						$prime_reaction[$counter]=$low[$counter];
						//$sub_reaction[$counter]=0.0;
						$sub_reaction[$counter]="";
						$price_position="e";
						$last_prime_reaction_price=$prime_reaction[$counter];
					}
					else
					{
						if($low[$counter]<$last_sub_reaction_price)
						{
							$trigger[$counter]=1.0;
							$sub_rally[$counter]="";
							$prime_rally[$counter]="";
							$main_rally[$counter]="";
							//$main_reaction[$counter]=0.0;
							$main_reaction[$counter]="";
							//$prime_reaction[$counter]=0.0;
							$prime_reaction[$counter]="";
							$sub_reaction[$counter]=$low[$counter];
							$price_position="f";
							$last_sub_reaction_price=$sub_reaction[$counter];
						}
						else
						{
							//$main_reaction[$counter]=0.0;
							$main_reaction[$counter]="";
							//$prime_reaction[$counter]=0.0;
							$prime_reaction[$counter]="";
							//$sub_reaction[$counter]=0.0;
							$sub_reaction[$counter]="";
							if(($high[$counter]-$last_sub_reaction_price)>($last_sub_reaction_price*reverse))
							{
								if(($high[$counter]>$last_main_rally_price)||(($high[$counter]-$prime_rally_pivot_point)>($prime_rally_pivot_point*reverse*0.5)))
								{
									$trigger[$counter]=1.0;
									//$sub_rally[$counter]=0.0;
									$sub_rally[$counter]="";
									//$prime_rally[$counter]=0.0;
									$prime_rally[$counter]="";
									$main_rally[$counter]=$high[$counter];
									$price_position="c";
									$last_main_rally_price=$main_rally[$counter];
								}
								else
								{
									if($high[$counter]>$last_prime_rally_price)
									{
										$trigger[$counter]=1.0;
										//$sub_rally[$counter]=0.0;
										$sub_rally[$counter]="";
										$prime_rally[$counter]=$high[$counter];
										//$main_rally[$counter]=0.0;
										$main_rally[$counter]="";
										$price_position="b";
										$last_prime_rally_price=$prime_rally[$counter];
									}
									else
									{
										$trigger[$counter]=1.0;
										$sub_rally[$counter]=$high[$counter];
										//$prime_rally[$counter]=0.0;
										$prime_rally[$counter]="";
										//$main_rally[$counter]=0.0;
										$main_rally[$counter]="";
										$price_position="a";
										$last_sub_rally_price=$sub_rally[$counter];
									};
								};
							}
							else
							{
								$trigger[$counter]=0.0;
								//$sub_rally[$counter]=0.0;
								$sub_rally[$counter]="";
								//$prime_rally[$counter]=0.0;
								$prime_rally[$counter]="";
								//$main_rally[$counter]=0.0;
								$main_rally[$counter]="";
								$price_position="f";
							};
						};
					};
				};
				break;
			};
		};
	};
?>