### Разметка котировок EoD от [Yahoo](http://finance.yahoo.com/) методом Ливермора###

* Метод разметки описан в [Вики](https://bitbucket.org/Roman_Rich/livermore/wiki/Home)
* Перечень компаний, котировки которых нужно разметить, перечисляем в файле init.php
* Работоспособность проверена на PHP>=5.3
* V.1.0

### Установка ###

* Клонируем
* php main.php
* Все зависимости удовлетворены
* Базы данных не используются
* Разработчикам: в [трекере](https://bitbucket.org/Roman_Rich/livermore/issues?status=new&status=open&sort=-priority) задач периодически буду публиковать новые мысли

Copyright (c) 2014-2015, [ROMAN RICH](https://www.facebook.com/roman.i.rich)