<?php
    if(!is_dir("out")) mkdir("out",0777);
    if(!is_dir("out/bonds")) mkdir("out/bonds",0777);
    if(!is_dir("out/commodities")) mkdir("out/commodities",0777);
    if(!is_dir("out/currencies")) mkdir("out/currencies",0777);
    if(!is_dir("out/markets")) mkdir("out/markets",0777);
    if(!is_dir("out/sectors")) mkdir("out/sectors",0777);
    if(!is_dir("out/stocks")) mkdir("out/stocks",0777);
    if(!is_dir("out/stocks/us")) mkdir("out/stocks/us",0777);
    if(!is_dir("out/stocks/uk")) mkdir("out/stocks/uk",0777);
    if(!is_dir("out/stocks/euro")) mkdir("out/stocks/euro",0777);
?>