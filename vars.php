<?php
	define("reverse",0.2); // Reverse size, %
	define("offset",20); // Volume MA offset
	
	// Yahoo source data
	$ticker=array();
	$market=array();
	$name=array();
	$path=array();

	// Data structure
	$trigger=array();
	$date=array();
	$open=array();
	$high=array();
	$low=array();
	$close=array();
	$volume=array();
	$strengt_of_volume=array();
	$sub_rally=array();
	$prime_rally=array();
	$main_rally=array();
	$main_reaction=array();
	$prime_reaction=array();
	$sub_reaction=array();

	// Strategy variable
	$price_position="";
	$last_sub_rally_price=0.0;
	$last_prime_rally_price=0.0;
	$last_main_rally_price=0.0;
	$last_main_reaction_price=1000000.0;
	$last_prime_reaction_price=1000000.0;
	$last_sub_reaction_price=1000000.0;
	$main_rally_pivot_point=1000000.0;
	$prime_rally_pivot_point=1000000.0;
	$main_reaction_pivot_point=0.0;
	$prime_reaction_pivot_point=0.0;
	$volume_sum=0.0;
	
	// Tip variable
	define("trend_sensitivity",reverse/10.0); // The distance from the junction in the trend
	define("reverse_sensitivity",reverse/2.0); // The distance from the junction of the trend
	$tip_in_rally_ticker=array();
	$tip_out_rally_ticker=array();
	$tip_in_reaction_ticker=array();
	$tip_out_reaction_ticker=array();
	$tip_in_rally_name=array();
	$tip_out_rally_name=array();
	$tip_in_reaction_name=array();
	$tip_out_reaction_name=array();
	$tip_in_rally_html=array();
	$tip_out_rally_html=array();
	$tip_in_reaction_html=array();
	$tip_out_reaction_html=array();
	$tip_in_rally_price=array();
	$tip_out_rally_price=array();
	$tip_in_reaction_price=array();
	$tip_out_reaction_price=array();
	$tip_in_rally_counter=0;
	$tip_out_rally_counter=0;
	$tip_in_reaction_counter=0;
	$tip_out_reaction_counter=0;
	$tip_trigger=false;

	// Other variable
	$link_num=0;
	$csv_read_trigger=false
?>