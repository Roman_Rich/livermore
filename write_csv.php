<?php
	// Write csv data
	if(($handle_csv=fopen("$path[$link_num]".".csv","w"))!==FALSE)
	{
		fputcsv($handle_csv,array($price_position,$last_sub_rally_price,$last_prime_rally_price,$last_main_rally_price,$last_main_reaction_price,$last_prime_reaction_price,$last_sub_reaction_price,$main_rally_pivot_point,$prime_rally_pivot_point,$main_reaction_pivot_point,$prime_reaction_pivot_point));
		fputcsv($handle_csv,array("T","Date","Open","High","Low","Close","Volume","SoV","SecondaryRally","NaturalRally","UpwardTrend","DownwardTrend","NaturalReaction","SecondaryReaction"));
		for($c=1;$c<=$row-offset;$c++)
		{
			fputcsv($handle_csv,array($trigger[$c],$date[$c],$open[$c],$high[$c],$low[$c],$close[$c],$volume[$c],$strengt_of_volume[$c],$sub_rally[$c],$prime_rally[$c],$main_rally[$c],$main_reaction[$c],$prime_reaction[$c],$sub_reaction[$c]));
		};
		fclose($handle_csv);
	};
?>