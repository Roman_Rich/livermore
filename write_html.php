<?php
	// Write html data
	if(($handle_html=fopen("$path[$link_num]".".html","w"))!==FALSE)
	{
		fputs($handle_html,'<!DOCTYPE html>'."\r\n");
		fputs($handle_html,'<html itemscope itemtype="http://schema.org/WebPage">'."\r\n");
		fputs($handle_html,"\t".'<head>'."\r\n");
		fputs($handle_html,"\t\t".'<meta charset=utf-8>'."\r\n");
		fputs($handle_html,"\t\t".'<title>'.$name[$link_num].'</title>'."\r\n");
		fputs($handle_html,"\t\t".'<meta name="description" content="Livermore charts" />'."\r\n");
		fputs($handle_html,"\t\t".'<style>'."\r\n");
		fputs($handle_html,"\t\t\t".'body'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'font: 10pt Liberation, mono;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'margin: 0;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t\t".'h1'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'font-size: 16px;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'margin: 0;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t\t".'TH'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'text-align: center;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'vertical-align: top;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'word-break: break-all;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t\t".'TD'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'text-align: right;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'vertical-align: top;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'word-break: break-all;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t\t".'#fixblock'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'background: white;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'width: 100%;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'height: 105px;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'text-align: center;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'position: fixed;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t\t".'#content'."\r\n");
		fputs($handle_html,"\t\t\t".'{'."\r\n");
		fputs($handle_html,"\t\t\t\t".'width: 100%;'."\r\n");
		fputs($handle_html,"\t\t\t\t".'padding-top: 110px;'."\r\n");
		fputs($handle_html,"\t\t\t".'}'."\r\n");
		fputs($handle_html,"\t\t".'</style>'."\r\n");
		fputs($handle_html,"\t".'</head>'."\r\n");
		fputs($handle_html,"\t".'<body>'."\r\n");

		fputs($handle_html,"\t\t".'<div id="fixblock">'."\r\n");
		fputs($handle_html,"\t\t\t".'<p><h1>'.$name[$link_num].'</h1></p>'."\r\n");
		fputs($handle_html,"\t\t\t".'<table width="1075" align="center" cols="14" border="1">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="50">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="100">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="100">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<tr>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>T</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>Date</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>Open</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>High</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>Low</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>Close</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>Volume</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>SoV</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>rally</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>RALLY</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>UP</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>DOWN</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>REACT</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<th>reac</th>'."\r\n");
		fputs($handle_html,"\t\t\t\t".'</tr>'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<tr>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td></td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_sub_rally_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_prime_rally_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_main_rally_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_main_reaction_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_prime_reaction_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t\t".'<td>'.$last_sub_reaction_price.'</td>'."\r\n");
		fputs($handle_html,"\t\t\t\t".'</tr>'."\r\n");
		fputs($handle_html,"\t\t\t".'</table>'."\r\n");
		fputs($handle_html,"\t\t".'</div>'."\r\n");
		
		fputs($handle_html,"\t\t".'<div id="content">'."\r\n");

		fputs($handle_html,"\t\t\t".'<table width="1075" align="center" cols="14" border="1">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="50">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="100">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="100">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");
		fputs($handle_html,"\t\t\t\t".'<col width="75">'."\r\n");

		for($c=1;$c<=$row-offset;$c++)
		{
			if($trigger[$c]==1.0)
			{
				fputs($handle_html,"\t\t\t\t".'<tr>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.$trigger[$c].'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.$date[$c].'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($open[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($high[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($low[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($close[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($volume[$c],0,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($strengt_of_volume[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($sub_rally[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($prime_rally[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($main_rally[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($main_reaction[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($prime_reaction[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t\t".'<td>'.number_format($sub_reaction[$c],2,'.',' ').'</td>'."\r\n");
				fputs($handle_html,"\t\t\t\t".'</tr>'."\r\n");
			};
		};

		fputs($handle_html,"\t\t\t".'</table>'."\r\n");
		fputs($handle_html,"\t\t".'</div>'."\r\n");
		
		fputs($handle_html,"\t".'</body>'."\r\n");
		fputs($handle_html,'</html>'."\r\n");
		fclose($handle_html);
	};
?>